﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTestSparta.Clases
{
    //En un desarrollo real heredaría de una interfaz IProducto
    class Producto_Precio
    {
        public int IdProducto { get; set; } 
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Group { get; set; }
        public string Prize { get; set; }

        //Para las childRows, se podría utilizar, en este caso, la misma clase Producto_Precio por ser 
        //las filas hijas igual que la padre, pero por limpieza mejor hacer 2 clases diferenciadas.
        private List<SubProducto_Precio> subproductos = new List<SubProducto_Precio>();

        //Lo hacemos global para evitar repetición de valores en los diferentes productos.
        private static Random rPrize = new Random();
        public List<SubProducto_Precio> Subproductos {
            get { return subproductos; }
            set {subproductos = value; } 
        }

        public Producto_Precio()
        {
        }

        #region Comentarios sobre la clase
        //Clase independiente que contiene los datos de los productos de la segunda grid.
        //Los procedimientos existentes crean los datos a piñon, si fuera un desarrollo real
        //su creación no estaría aquí, se crearía alguna clase para establecer la comunicación con el WS o BDD
        //que los tuviera y se asignarían los valores devueltos a la clase.
        //Si hubiera implicados WS o BDD la URL / Cadena de conexión debería estar en el config
        #endregion
        /// <summary>
        /// Procedimiento que permite la carga inicial "a fuego" de los productos de la segunda grid
        /// </summary>
        /// <returns>Lista de productos</returns>
        public static List<Producto_Precio> CargaInicial()
        {
            
            List<Producto_Precio> lp = new List<Producto_Precio>();
            Producto_Precio p = new Producto_Precio();
            
            p = CreateProducto(0,"Jun 2020 - Product1", "Group1",new DateTime(2020,01,01));
            p.Subproductos.Add(SubProducto_Precio.CreateSubProduct(0));
            p.Subproductos.Add(SubProducto_Precio.CreateSubProduct(1));
            lp.Add(p);

            p = CreateProducto(1,"Feb 2020 - F5 325 Kbbls", "Group2",new DateTime(2020, 01, 01));
            p.Subproductos.Add(SubProducto_Precio.CreateSubProduct(0));
            lp.Add(p);

            p = CreateProducto(2,"Mar 2020 - Nigeria 91R 1000ppm 33kt", "Group2", new DateTime(2020, 01, 01));
            lp.Add(p);

            p = CreateProducto(3,"Feb 2020 - Reformate {102r/50kpa} 12 kt", "Group2", new DateTime(2020, 01, 16));
            lp.Add(p);
            return lp;
        }
        /// <summary>
        /// Procedimiento para la creación de un producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="group"></param>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static Producto_Precio CreateProducto(int id, string name, string group, DateTime fecha)
        {
            double min_value = 1;
            double max_value = 30;
            
            Producto_Precio p = new Producto_Precio();

            p.IdProducto = id;
            p.Name = name;

            p.Date = fecha;
            p.Group = group;

            //Lo definimos así porque sino, por defecto, el símbolo $ aparece por delante de la cifra
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi.CurrencyPositivePattern = 3; // XX.XX $
            p.Prize = string.Format(nfi, "{0:c}",(Math.Round(Convert.ToDecimal(rPrize.NextDouble() * (max_value - min_value) + min_value), 2)));

            return p;
        }
    }
}
