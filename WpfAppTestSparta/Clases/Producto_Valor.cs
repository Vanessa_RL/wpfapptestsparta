﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfAppTestSparta.Clases
{
    //Clase que contendrá todos los datos de un producto.
    //En un desarrollo real heredaría de una interfaz IProducto
    class Producto_Valor
    {
        public int IdProducto { get; set; } 

        //Valores iniciales, se guardan en una propiedad para que puedan ser columnas de la grid
        //y así poder comparar el valor con los nuevos y modificar el estilo de la celda correspondiente.
        //Son columnas ocultas
        public decimal BaseJan2020Data { get; set; }
        public decimal BaseFeb2020Data { get; set; }
        public decimal BaseMar2020Data { get; set; }
        public decimal BaseApr2020Data { get; set; }
        public decimal BaseMay2020Data { get; set; }
        public decimal BaseJun2020Data { get; set; }

        public string Name { get; set; }

        public decimal Jan2020Data { get; set; }
        public decimal Feb2020Data { get; set; }
        public decimal Mar2020Data { get; set; }
        public decimal Apr2020Data { get; set; }
        public decimal May2020Data{ get; set; }
        public decimal Jun2020Data { get; set; }


        //Lo declaramos global para evitar que se repitan los valores
        private static Random rMonthData = new Random(); 

        public Producto_Valor()
        {


        }
        #region Comentarios sobre la clase
        //Clase independiente que contiene los datos de los productos de la primera grid.
        //Los procedimientos existentes crean los datos a piñon, si fuera un desarrollo real
        //su creación no estaría aquí, se crearía alguna clase para establecer la comunicación con el WS o BDD
        //que los tuviera y se asignarían los valores devueltos a la clase.
        //Si hubiera implicados WS o BDD la URL / Cadena de conexión debería estar en el config
        #endregion

        /// <summary>
        /// Procedimiento que crea un producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Producto_Valor CreateProduct(int id)
        {
            double min_value = -99.99;
            double max_value = 99.99;
            

            Producto_Valor p = new Producto_Valor();
            p.IdProducto = id;
            p.Name = "Product" + id;

            p.Jan2020Data = p.BaseJan2020Data =  Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);
            p.Feb2020Data = p.BaseFeb2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value),2);
            p.Mar2020Data = p.BaseMar2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value),2);
            p.Apr2020Data = p.BaseApr2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value),2);
            p.May2020Data = p.BaseMay2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value),2);
            p.Jun2020Data = p.BaseJun2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value),2);

            return p;
        }

        /// <summary>
        /// Procedimiento que recalcula el valor/mes de un produto. Al actualizar el valor de la propiedad 
        /// se actualiza automáticamente el valor en la grid
        /// </summary>
        public void ActualizaValorProducto()
        {
            IDictionary<string, decimal> newValues = new Dictionary<string, decimal>();

            double min_value = -99.99;
            double max_value = 99.99;

            this.Jan2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);
            this.Feb2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);
            this.Mar2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);
            this.Apr2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);
            this.May2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);
            this.Jun2020Data = Math.Round(Convert.ToDecimal(rMonthData.NextDouble() * (max_value - min_value) + min_value), 2);

        }
        /// <summary>
        /// Procedimiento que realiza la carga inicial de la primera grid creando 5 productos.
        /// </summary>
        /// <returns>Lista de productos</returns>

        public static List<Producto_Valor> CargaInicial()
        {
            
            List<Producto_Valor> lp = new List<Producto_Valor>();
            

            for (int i = 0; i < 5; i++) 
            {
                Producto_Valor p = CreateProduct(i);             
                lp.Add(p);
            }
            return lp;
        }


    }
}
