﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppTestSparta.Clases
{
    //En un desarrollo real heredaría de una interfaz IProducto
    class SubProducto_Precio
    {
        public int IdProducto { get; set; } 
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Group { get; set; }
        public string Prize { get; set; }
  
        public SubProducto_Precio()
        {
        }

        /// <summary>
        /// Procedimiento para crear un subproducto de la segunda grid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static SubProducto_Precio CreateSubProduct(int id)
        {
            double min_value = 1;
            double max_value = 30;
            Random rPrize = new Random();

            SubProducto_Precio sp = new SubProducto_Precio();
            sp.IdProducto = id;
            sp.Name = "Product" + id;

            sp.Date = new DateTime(2020, 01, 01, 12, 45, 00);
            sp.Group = "Group1";

            //Lo definimos así porque sino, por defecto, el símbolo $ aparece por delante de la cifra
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi.CurrencyPositivePattern = 3; //XX.XX $
            sp.Prize = string.Format(nfi, "{0:c}", (Math.Round(Convert.ToDecimal(rPrize.NextDouble() * (max_value - min_value) + min_value), 2)));

            return sp;
        }
    }
}
