﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using WpfAppTestSparta.Clases;

namespace WpfAppTestSparta
{
    public class NumericComparisonConverter :  IMultiValueConverter
    {
       
        /// <summary>
        /// Procedimiento que permite modificar la apariencia de una celda a partir de la comparación de valores de 2 celdas.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                
                decimal valorBase = values[0] == null ? 0: System.Convert.ToDecimal(values[0]);
                decimal  nuevoValor = values[1] == null ? 0 : System.Convert.ToDecimal(values[1]);

                if (nuevoValor == valorBase)
                    return new SolidColorBrush(Colors.Black);
                else if (nuevoValor > valorBase)
                    return new SolidColorBrush(Colors.Red);
                else
                    return new SolidColorBrush(Colors.Green);

            }catch (Exception ex)
            {
                //Si hubiera gestión de logs dejar traza para identificar un posible valor erróneo
                return new SolidColorBrush(Colors.Black);
            }

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }

}
