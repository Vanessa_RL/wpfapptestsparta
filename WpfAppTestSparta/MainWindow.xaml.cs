﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfAppTestSparta.Clases;
using System.Windows.Controls.Primitives;

namespace WpfAppTestSparta
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Carga inicial de productos de la grid y asignación de ItemsSource para que se visualicen
        /// </summary>
        private void InitializeGridProductosValor()
        {
            productoValorList = Producto_Valor.CargaInicial();
            this.GridProductos.ItemsSource = productoValorList;
        }

        /// <summary>
        /// Carga inicial de productos de la segunda grid y asignación de ItemsSource para que se visualicen
        /// </summary>
        private void InitializeGridProductosPrecio()
        {
            productPrecioList = Producto_Precio.CargaInicial();
            this.GridSubProductos.ItemsSource = productPrecioList;
           
        }

        private List<Producto_Valor> productoValorList = new List<Producto_Valor>();
        private List<Producto_Precio> productPrecioList = new List<Producto_Precio>();

        //Timer que nos permite recalcular el valor/mes de los productos cada 5 segundos
        private DispatcherTimer tmUpdateValores = new DispatcherTimer();

        //Se inicializan a nivel de datos las grids, no se puede hacer en InitializeComponent ya que
        //ItemsSource no está disponible
        //Inicialización del timer
        private void VentanaPrincipal_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeGridProductosValor();
            InitializeGridProductosPrecio();
            //Configuramos el timer para ir a obtener nuevos valores de productos cada 5 segundos
            tmUpdateValores.Interval = new TimeSpan(0, 0, 5);
            tmUpdateValores.Tick += TmUpdateValores_Tick1;
            tmUpdateValores.Start();

        }

        private void ActualizarValores()
        {
            //Se podría deshabilitar la grid mientras se hacen los cálculos para evitar que el usuario interactue
            //podría ser interesante mostrar un popUp con un BackGroundworker pero siendo cada 5 segundos puede resultar pesado
            IDictionary<string, decimal> newValues = new Dictionary<string, decimal>();

            //Para cada productos de la primera grid actualizamos su valor/mes
            foreach (Producto_Valor pv in productoValorList)
            {
                pv.ActualizaValorProducto();
               
            }
            this.GridProductos.Items.Refresh();
        }


        private void TmUpdateValores_Tick1(object sender, EventArgs e)
        {
            ActualizarValores();
        }

        //Al pulsar el botón creamos un nuevo producto y lo añadimos a la grid
        private void btnNewProduct_Click(object sender, RoutedEventArgs e)
        {
            int maxId = productoValorList.Max(r => r.IdProducto);
            Producto_Valor newProduct = Producto_Valor.CreateProduct(maxId + 1);
            productoValorList.Add(newProduct);
            this.GridProductos.Items.Refresh();
        }

        //Comportamiento al pulsar el botón de expandir/contraer en modo expansión en la segunda grid.
        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    Producto_Precio p = row.DataContext as Producto_Precio;

                    //Si no hay childRows no se hace nada
                    if(p.Subproductos.Count == 0)
                        row.DetailsVisibility =  Visibility.Collapsed ;
                    else
                        row.DetailsVisibility = row.DetailsVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
                   
                    break;
                }
        }

        //Comportamiento al pulsar el botón de expandir/contraer en modo contracción en la segunda grid.
        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    Producto_Precio p = row.DataContext as Producto_Precio;

                    //Si no hay childRows no se hace nada
                    if (p.Subproductos.Count == 0)
                        row.DetailsVisibility = Visibility.Collapsed;
                    else
                        row.DetailsVisibility = row.DetailsVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
                    break;
                }
        }
    }
}
